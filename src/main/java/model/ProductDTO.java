package model;

public class ProductDTO {
	private int id;
	private String title;
	private int amount;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public ProductDTO() {
		// this form used by Hibernate
	}

	public ProductDTO(int id, String title, int amount) {
		// for application use, to create new events
		this.id = id;
		this.title = title;
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
