package database;

public interface Setup 
{
	boolean openConnection();
	
	boolean closeConnection();
	
	boolean createDatabase(String database);
	
	boolean createTable(String nameOfTable);
	
	boolean setUniqueKey(String tableName, String tableField);
}
