package sqlite;

public class SqliteCreator {
	private Boolean openConnection;
	private Boolean createTable;
	private Boolean closeConnection;
	SqliteSetup sqliteSetup = new SqliteSetup();
	SqliteOperations sqliteOperations = new SqliteOperations();

	public void createDB(String tableName) {
		openConnection = sqliteSetup.openConnection();
		createTable = sqliteSetup.createTable(tableName);
		closeConnection = sqliteSetup.closeConnection();
	}

	public void insertElementToTable(String nameOfTable, String givenName, String givenSurname, String givenBirthDate) {
		sqliteSetup.openConnection();
		sqliteOperations.setStatement(sqliteSetup.getStatement());
		// sqliteOperations.insertAction( query);
		sqliteSetup.closeConnection();
	}

	public Boolean getOpenConnection() {
		return openConnection;
	}

	public Boolean getCreateTable() {
		return createTable;
	}

	public Boolean getCloseConnection() {
		return closeConnection;
	}

}
