package sqlite;

import java.sql.SQLException;
import java.sql.Statement;

import database.Operations;
import utils.Dictionary;

public class SqliteOperations implements Operations {
	private Statement stat;

	public boolean insertAction(String query) {
		StringBuilder sbInsert = new StringBuilder("INSERT INTO ");
		// sbInsert = appendToStringBuilder(sbInsert, nameOfTable, givenName,
		// givenSurname, givenBirthDate);
		try {
			stat.execute(sbInsert.toString());
		} catch (SQLException e) {
			System.err.println(Dictionary.ERROR_INSERTING_ELEMENT);
			e.printStackTrace();
			System.out.println("Insertion action failed.");

			StringBuilder sbReaplce = new StringBuilder("REPLACE INTO ");
			// sbReaplce = appendToStringBuilder(sbReaplce, nameOfTable, givenName,
			// givenSurname, givenBirthDate);
			try {
				stat.execute(sbReaplce.toString());
				System.out.println("Replation action successful.");
			} catch (SQLException ex) {
				System.err.println(Dictionary.ERROR_REPLACING_ELEMENT);
				ex.printStackTrace();
				return false;
			}

			return true;
		}

		return true;
	}

	public boolean updateAction(String query) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean deleteAction(String query) {
		// TODO Auto-generated method stub
		return false;
	}

	public void searchAction(String query) {
		// TODO Auto-generated method stub

	}

	private StringBuilder appendToStringBuilder(StringBuilder sb, String nameOfTable, String givenName,
			String givenSurname, String givenBirthDate) {
		sb.append(nameOfTable).append(" (name, surname, birthdate) VALUES ('").append(givenName).append("','")
				.append(givenSurname).append("','").append(givenBirthDate).append("')");

		return sb;
	}

	public void setStatement(Statement stat) {
		this.stat = stat;
	}

	public boolean switchDatabase(String nameOfDatabase) {
		// TODO Auto-generated method stub
		return false;
	}

}
