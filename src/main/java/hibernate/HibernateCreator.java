package hibernate;

import org.hibernate.SessionFactory;

public class HibernateCreator {

	private SessionFactory sessionFactory;

	public void testHibernate() {
		NativeHibernateViaXML nativeHibernateViaXML = new NativeHibernateViaXML();
		try {
			nativeHibernateViaXML.setUp();
			nativeHibernateViaXML.testBasicUsage();
			nativeHibernateViaXML.tearDown();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
