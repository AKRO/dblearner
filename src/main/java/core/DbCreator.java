package core;

import hibernate.HibernateCreator;

public class DbCreator {
	static String database = "sklep2";
	static String table = "przedmioty";
	static String name = "Kamilos";
	static String surname = "KOKU";
	static String pesel = "1111199911233";
	static String tableValues = "ilosc int, tytul varchar(255)";
	static String changeAmount = "ilosc 20, tytul tomato";
	static String changeTitle = "tytul tomtoesies, tytul tomato";
	static String tableColumns = "ilosc, tytul";
	static String insertValues = "8, 'potato'";
	static String deleteValueName = "tytul potatoes";
	static String deleteValueQuantity = "ilosc 20";

	public static void main(String[] args) {
		/*
		 * SqliteCreator sqliteCreator = new SqliteCreator();
		 * sqliteCreator.createDB(database); sqliteCreator.insertElementToTable(
		 * database, name, surname, pesel);
		 */

		// MySqlCreator mySQLCreator = new MySqlCreator();
		// mySQLCreator.createDB(database);
		// mySQLCreator.createTable(database, table, tableValues);
		// mySQLCreator.insertElementToTable(database, table, insertValues);
		// mySQLCreator.selectElement(database, table, tableColumns);
		// mySQLCreator.updateElement(database, table, changeAmount);
		// mySQLCreator.updateElement(database, table, changeTitle);
		// mySQLCreator.deleteElement(database, table, deleteValueName);
		// mySQLCreator.deleteElement(database, table, deleteValueQuantity);

		HibernateCreator hibernateCreator = new HibernateCreator();
		hibernateCreator.testHibernate();
	}

}
