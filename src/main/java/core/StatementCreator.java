package core;

import java.util.HashMap;
import java.util.Map;

import utils.Dictionary;
import utils.QueryParts;

public class StatementCreator {
	public String prepareStatement(int actionToPrepare, String values, String tableName) {
		StringBuilder sb = new StringBuilder();

		switch (actionToPrepare) {
		// create db
		case 1:
			// "create database if not exists " + db + ";"
			sb.append(QueryParts.CREATE + QueryParts.SPACE + QueryParts.DATABASE + QueryParts.SPACE
					+ QueryParts.IF_NOT_EXISTS + QueryParts.SPACE + values + QueryParts.SEMICOLON);
			break;

		// create table
		case 2:
			// "create table " + nameOfTable + " (" +
			// prepareTableColumns()
			// + ");"

			sb.append(QueryParts.CREATE + QueryParts.SPACE + QueryParts.TABLE + QueryParts.SPACE + tableName
					+ QueryParts.PARENTHESIS_OPEN);
			HashMap<String, String> hmC = distinguishNameFromValue(values);
			sb = fillSbForCreate(sb, hmC);
			sb.append(QueryParts.PARENTHESIS_CLOSE + QueryParts.SEMICOLON);
			break;

		// retrieve data (select operation)
		case 3:
			sb.append(QueryParts.SELECT + QueryParts.SPACE + values + QueryParts.SPACE + QueryParts.FROM
					+ QueryParts.SPACE + tableName + QueryParts.SEMICOLON);
			break;

		// insert data
		case 4:
			// "insert into books values (1001, 'Java for dummies', 'Tan Ah Teck', 11.11)";
			sb.append(QueryParts.INSERT_INTO + QueryParts.SPACE + tableName + QueryParts.SPACE + QueryParts.VALUES
					+ QueryParts.SPACE + QueryParts.PARENTHESIS_OPEN + values + QueryParts.PARENTHESIS_CLOSE
					+ QueryParts.SEMICOLON);
			break;

		// update data
		case 5:
			// UPDATE table SET ilosc=1 WHERE tytul='potato'
			sb.append(QueryParts.UPDATE + QueryParts.SPACE + tableName + QueryParts.SPACE + QueryParts.SET
					+ QueryParts.SPACE);
			HashMap<String, String> hmU = distinguishNameFromValue(values);
			sb = fillSbForUpdate(sb, hmU, true);
			sb.append(QueryParts.SEMICOLON);
			break;

		// delete data
		case 6:
			// DELETE FROM employees WHERE officeCode = 4;
			sb.append(QueryParts.DELETE + QueryParts.SPACE + QueryParts.FROM + QueryParts.SPACE + tableName
					+ QueryParts.SPACE + QueryParts.WHERE + QueryParts.SPACE);
			HashMap<String, String> hmD = distinguishNameFromValue(values);
			sb = fillSbForUpdate(sb, hmD, false);
			sb.append(QueryParts.SEMICOLON);
			break;
		}

		return sb.toString();
	}

	private HashMap<String, String> distinguishNameFromValue(String values) {
		HashMap<String, String> nameAndType = new HashMap<String, String>();
		String[] listOfPairs = values.split(",");
		for (String str : listOfPairs) {
			String trimmed = str.trim();
			String[] key_value = trimmed.split(" ");
			if (!nameAndType.containsKey(key_value[0])) {
				nameAndType.put(key_value[0].trim(), key_value[1].trim());
			} else {
				String secondOccurence = key_value[0].trim() + Dictionary.UNIQUE_VAL_FOR_DUPL;
				nameAndType.put(secondOccurence, key_value[1].trim());
			}
		}

		return nameAndType;
	}

	private StringBuilder fillSbForCreate(StringBuilder sb, HashMap<String, String> hm) {
		for (Map.Entry<String, String> entry : hm.entrySet()) {
			sb.append(entry.getKey());
			sb.append(QueryParts.SPACE);
			sb.append(entry.getValue());
			sb.append(QueryParts.COMMA);
		}
		sb.deleteCharAt(sb.length() - 1);

		return sb;
	}

	private StringBuilder fillSbForUpdate(StringBuilder sb, HashMap<String, String> hm, boolean flag) {
		for (Map.Entry<String, String> entry : hm.entrySet()) {
			if (entry.getKey().endsWith(Dictionary.UNIQUE_VAL_FOR_DUPL)) {
				sb.append(entry.getKey().substring(0, entry.getKey().length() - 2));
			} else {
				sb.append(entry.getKey());
			}
			sb.append(QueryParts.EQUALLS);
			sb.append(QueryParts.QUOTE_SINGLE);
			sb.append(entry.getValue());
			sb.append(QueryParts.QUOTE_SINGLE);
			if (flag) {
				sb.append(QueryParts.SPACE);
				sb.append(QueryParts.WHERE);
				sb.append(QueryParts.SPACE);
				flag = false;
			}
		}

		return sb;
	}

}
