package mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import database.Setup;
import utils.Dictionary;

public class MySqlSetup implements Setup {
	private String dbDriver = Dictionary.MYSQL_DRIVER;
	private Connection conn;
	private Statement stmt;

	public boolean openConnection() {
		try {
			conn = DriverManager.getConnection(Dictionary.MYSQL_DB_FILE_PATH_LOCALHOST, Dictionary.MYSQL_DB_USER,
					Dictionary.MYSQL_DB_PASSWORD);
			// For MySQL only
			// The format is: "jdbc:mysql://hostname:port/databaseName", "username",
			// "password"
			stmt = conn.createStatement();
		} catch (SQLException e) {
			System.err.println(Dictionary.CONNECTION_WITH_DB_FAILURE);
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			System.err.println(Dictionary.ERROR_CLOSING_CONNECTION);
			e.printStackTrace();
			return false;
		}
		return false;
	}

	public boolean createDatabase(String query) {
		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean createTable(String query) {
		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean renameTable(String query) {
		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean setUniqueKey(String tableName, String tableField) {
		// TODO Auto-generated method stub
		return false;
	}

	public Statement getStatement() {
		return stmt;
	}

}
