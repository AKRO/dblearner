package mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.Operations;
import utils.Dictionary;
import utils.QueryParts;

public class MySqlOperationsDAO implements Operations {

	private Statement stmt;
	String result = null;

	public boolean insertAction(String query) {
		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public boolean updateAction(String query) {
		try {
			System.out.println(Dictionary.SQL_STATMNT_IS + query + "\n"); // Echo For debugging

			stmt.executeUpdate(query);
		} catch (Exception exc) {
			exc.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean deleteAction(String query) {
		try {
			System.out.println(Dictionary.SQL_STATMNT_IS + query + "\n"); // Echo For debugging

			stmt.executeUpdate(query);
		} catch (Exception exc) {
			exc.printStackTrace();
			return false;
		}

		return true;
	}

	public void searchAction(String query) {

		try {
			System.out.println(Dictionary.SQL_STATMNT_IS + query + "\n"); // Echo For debugging

			ResultSet rset = stmt.executeQuery(query);

			int rowCount = 0;
			while (rset.next()) { // Move the cursor to the next row, return false if no more row
				String what = rset.getString("tytul");
				int amount = rset.getInt("ilosc");
				System.out.println(what + " | " + amount);
				++rowCount;
			}
			System.out.println(Dictionary.TOTAL_NUMBER_OF_RECORDS + rowCount);

		} catch (SQLException ex) {
			ex.printStackTrace();
		} // Step 5: Close conn and stmt - Done automatically by try-with-resources (JDK
			// 7)
	}

	public void setStatement(Statement stmt) {
		this.stmt = stmt;
	}

	public boolean switchDatabase(String nameOfDb) {
		try {

			stmt.execute(QueryParts.USE + QueryParts.SPACE + nameOfDb + QueryParts.SEMICOLON);
		} catch (SQLException e) {

		}
		return false;
	}

}
