package mysql;

import core.StatementCreator;

public class MySqlCreator {
	private String query;
	private Boolean openConnection;
	private Boolean createTable;
	private Boolean closeConnection;
	MySqlSetup mySqlSetup = new MySqlSetup();
	MySqlOperationsDAO mySqlOperations = new MySqlOperationsDAO();
	StatementCreator statementCreator = new StatementCreator();

	public void createDB(String dbName) {
		query = statementCreator.prepareStatement(1, dbName, "");
		mySqlSetup.openConnection();
		mySqlSetup.createDatabase(query);
		mySqlSetup.closeConnection();
	}

	public void createTable(String dbName, String tableName, String tableValues) {

		mySqlSetup.openConnection();
		mySqlOperations.setStatement(mySqlSetup.getStatement());
		mySqlOperations.switchDatabase(dbName);
		query = statementCreator.prepareStatement(2, tableValues, tableName);
		mySqlSetup.createTable(query);
		mySqlSetup.closeConnection();
	}

	public void selectElement(String dbName, String tableName, String tableValues) {
		mySqlSetup.openConnection();
		mySqlOperations.setStatement(mySqlSetup.getStatement());
		mySqlOperations.switchDatabase(dbName);
		query = statementCreator.prepareStatement(3, tableValues, tableName);
		mySqlOperations.searchAction(query);
		mySqlSetup.closeConnection();
	}

	public void insertElementToTable(String dbName, String tableName, String tableValues) {
		mySqlSetup.openConnection();
		mySqlOperations.setStatement(mySqlSetup.getStatement());
		mySqlOperations.switchDatabase(dbName);
		query = statementCreator.prepareStatement(4, tableValues, tableName);
		mySqlOperations.insertAction(query);
		mySqlSetup.closeConnection();
	}

	public void updateElement(String dbName, String tableName, String tableValues) {
		mySqlSetup.openConnection();
		mySqlOperations.setStatement(mySqlSetup.getStatement());
		mySqlOperations.switchDatabase(dbName);
		query = statementCreator.prepareStatement(5, tableValues, tableName);
		mySqlOperations.updateAction(query);
		mySqlSetup.closeConnection();
	}

	public void deleteElement(String dbName, String tableName, String tableValues) {
		mySqlSetup.openConnection();
		mySqlOperations.setStatement(mySqlSetup.getStatement());
		mySqlOperations.switchDatabase(dbName);
		query = statementCreator.prepareStatement(6, tableValues, tableName);
		mySqlOperations.deleteAction(query);
		mySqlSetup.closeConnection();
	}

	public Boolean getOpenConnection() {
		return openConnection;
	}

	public Boolean getCreateTable() {
		return createTable;
	}

	public Boolean getCloseConnection() {
		return closeConnection;
	}

}
