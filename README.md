# WIP: dbLearner

I am teaching myself how to create a db and check out the differences between sqlite/mysql/postgresql etc. Later i will be adding Hibernate and REST/SOAP web services, hopefully even Spring if possible. The db can be used later in other projects and most probably i will create a basic UI with JavaFX.

Notes:
*  sqlite - it turns out it's really easy and straight forward. Basically nothing is needed besides the connector.
*  mysql - what i understand for now is that it's used when security is important. Logs in with user+password and a server is required. (connector + mysql community installer needs downloading)

Personal lessons/notes:
DTO/DAO/Entity - A class with the @Entity adnotation is the literal represantation of a single record in a db table. It has all the fields, even those that are not supposed to be shown publicly (like passwords). The DataTransferObject is a copy of the entity with only the fields that are needed during an operation. We create an instance of a DTO with data from the db that is not fragile and pass it to the class that needs it. The DataAccessObject has all the operations needed to retrieve an entity from the db, in order to create the DTO which will be used in the program.